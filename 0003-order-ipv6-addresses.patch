From a408ae347d5fca157e4cf8d9a54b851868f25aa2 Mon Sep 17 00:00:00 2001
From: Thomas Haller <thaller@redhat.com>
Date: Wed, 4 May 2022 15:22:33 +0200
Subject: [PATCH 1/1] Revert changes to order of IPv6 addresses

In upstream 1.39.2 and 1.37.91 we changed the order of IPv6 addresses.
The order affects source address selection.

There are two changes upstream:

- now we would configure manual addresses before DHCPv6 addresses
  before autoconf6 addresses.
- the order of addresses within one group also swapped. That is,
  for manual "ipv6.addresses", previously the last addresss was
  the highest priority one. Likewise, when receiving autoconf6 addresses
  or DHCPv6 addresses the order got corrected, so that the first address
  is the first.

For RHEL, we don't want to have this change, so revert.

This reverts commit 3d6b6aa31728a36af491ebcd4964a0e1a3ada27d.
This reverts commit 4a548423b91ebaf590700926709bb95a32a21e99.
---
 src/libnm-core-impl/nm-setting-ip6-config.c   |  2 +-
 src/libnm-platform/nm-platform.c              | 21 +++++++------------
 src/libnmc-setting/settings-docs.h.in         |  2 +-
 .../generate-docs-nm-settings-nmcli.xml.in    |  2 +-
 4 files changed, 10 insertions(+), 17 deletions(-)

diff --git a/src/libnm-core-impl/nm-setting-ip6-config.c b/src/libnm-core-impl/nm-setting-ip6-config.c
index f75c14ce5280..dc14db6c4cdb 100644
--- a/src/libnm-core-impl/nm-setting-ip6-config.c
+++ b/src/libnm-core-impl/nm-setting-ip6-config.c
@@ -946,7 +946,7 @@ nm_setting_ip6_config_class_init(NMSettingIP6ConfigClass *klass)
      * format: a comma separated list of addresses
      * description: A list of IPv6 addresses and their prefix length. Multiple addresses
      * can be separated by comma. For example "2001:db8:85a3::8a2e:370:7334/64, 2001:db8:85a3::5/64".
-     * The addresses are listed in decreasing priority, meaning the first address will
+     * The addresses are listed in increasing priority, meaning the last address will
      * be the primary address. This can make a difference with IPv6 source address selection
      * (RFC 6724, section 5).
      * ---end---
diff --git a/src/libnm-platform/nm-platform.c b/src/libnm-platform/nm-platform.c
index 8866a4367823..184ae15e0611 100644
--- a/src/libnm-platform/nm-platform.c
+++ b/src/libnm-platform/nm-platform.c
@@ -4087,7 +4087,8 @@ nm_platform_ip_address_sync(NMPlatform *self,
         }
     }
 
-    /* @known_addresses are in decreasing priority order (highest priority addresses first). */
+    /* @known_addresses (IPv4) are in decreasing priority order (highest priority addresses first).
+     * @known_addresses (IPv6) are in increasing priority order (highest priority addresses last) (we will sort them by scope next). */
 
     /* The order we want to enforce is only among addresses with the same
      * scope, as the kernel keeps addresses sorted by scope. Therefore,
@@ -4095,7 +4096,7 @@ nm_platform_ip_address_sync(NMPlatform *self,
      * unnecessary change the order of addresses with different scopes. */
     if (!IS_IPv4) {
         if (known_addresses)
-            g_ptr_array_sort_with_data(known_addresses, ip6_address_scope_cmp_descending, NULL);
+            g_ptr_array_sort_with_data(known_addresses, ip6_address_scope_cmp_ascending, NULL);
     }
 
     if (!_addr_array_clean_expired(addr_family,
@@ -4311,7 +4312,7 @@ nm_platform_ip_address_sync(NMPlatform *self,
             cur_scope              = IP6_ADDR_SCOPE_LOOPBACK;
             delete_remaining_addrs = FALSE;
             i_plat                 = plat_addresses->len;
-            i_know                 = nm_g_ptr_array_len(known_addresses);
+            i_know                 = 0;
 
             while (i_plat > 0) {
                 const NMPObject            *plat_obj  = plat_addresses->pdata[--i_plat];
@@ -4329,9 +4330,9 @@ nm_platform_ip_address_sync(NMPlatform *self,
                 }
 
                 if (!delete_remaining_addrs) {
-                    while (i_know > 0) {
+                    while (i_know < nm_g_ptr_array_len(known_addresses)) {
                         const NMPlatformIP6Address *know_addr =
-                            NMP_OBJECT_CAST_IP6_ADDRESS(known_addresses->pdata[--i_know]);
+                            NMP_OBJECT_CAST_IP6_ADDRESS(known_addresses->pdata[i_know++]);
                         IP6AddrScope know_scope;
 
                         if (!know_addr)
@@ -4368,21 +4369,13 @@ next_plat:;
     /* Add missing addresses. New addresses are added by kernel with top
      * priority.
      */
-    for (i = 0; i < known_addresses->len; i++) {
+    for (i_know = 0; i_know < known_addresses->len; i_know++) {
         const NMPObject            *plat_obj;
         const NMPObject            *known_obj;
         const NMPlatformIPXAddress *known_address;
         guint32                     lifetime;
         guint32                     preferred;
 
-        /* IPv4 addresses we need to add in the order most important first.
-         * IPv6 addresses we need to add in the reverse order with least
-         *   important first. Kernel will interpret the last address as most
-         *   important.
-         *
-         * @known_addresses is always in the order most-important-first. */
-        i_know = IS_IPv4 ? i : (known_addresses->len - i - 1u);
-
         known_obj = known_addresses->pdata[i_know];
         if (!known_obj)
             continue;
diff --git a/src/libnmc-setting/settings-docs.h.in b/src/libnmc-setting/settings-docs.h.in
index 2a582ad7a2a5..dc9e40db0224 100644
--- a/src/libnmc-setting/settings-docs.h.in
+++ b/src/libnmc-setting/settings-docs.h.in
@@ -255,7 +255,7 @@
 #define DESCRIBE_DOC_NM_SETTING_IP4_CONFIG_ROUTES N_("A list of IPv4 destination addresses, prefix length, optional IPv4 next hop addresses, optional route metric, optional attribute. The valid syntax is: \"ip[/prefix] [next-hop] [metric] [attribute=val]...[,ip[/prefix]...]\". For example \"192.0.2.0/24 10.1.1.1 77, 198.51.100.0/24\".")
 #define DESCRIBE_DOC_NM_SETTING_IP4_CONFIG_ROUTING_RULES N_("A comma separated list of routing rules for policy routing.")
 #define DESCRIBE_DOC_NM_SETTING_IP6_CONFIG_ADDR_GEN_MODE N_("Configure method for creating the address for use with RFC4862 IPv6 Stateless Address Autoconfiguration. The permitted values are: NM_SETTING_IP6_CONFIG_ADDR_GEN_MODE_EUI64 (0) or NM_SETTING_IP6_CONFIG_ADDR_GEN_MODE_STABLE_PRIVACY (1). If the property is set to EUI64, the addresses will be generated using the interface tokens derived from hardware address. This makes the host part of the address to stay constant, making it possible to track host's presence when it changes networks. The address changes when the interface hardware is replaced. The value of stable-privacy enables use of cryptographically secure hash of a secret host-specific key along with the connection's stable-id and the network address as specified by RFC7217. This makes it impossible to use the address track host's presence, and makes the address stable when the network interface hardware is replaced. On D-Bus, the absence of an addr-gen-mode setting equals enabling stable-privacy. For keyfile plugin, the absence of the setting on disk means EUI64 so that the property doesn't change on upgrade from older versions. Note that this setting is distinct from the Privacy Extensions as configured by \"ip6-privacy\" property and it does not affect the temporary addresses configured with this option.")
-#define DESCRIBE_DOC_NM_SETTING_IP6_CONFIG_ADDRESSES N_("A list of IPv6 addresses and their prefix length. Multiple addresses can be separated by comma. For example \"2001:db8:85a3::8a2e:370:7334/64, 2001:db8:85a3::5/64\". The addresses are listed in decreasing priority, meaning the first address will be the primary address. This can make a difference with IPv6 source address selection (RFC 6724, section 5).")
+#define DESCRIBE_DOC_NM_SETTING_IP6_CONFIG_ADDRESSES N_("A list of IPv6 addresses and their prefix length. Multiple addresses can be separated by comma. For example \"2001:db8:85a3::8a2e:370:7334/64, 2001:db8:85a3::5/64\". The addresses are listed in increasing priority, meaning the last address will be the primary address. This can make a difference with IPv6 source address selection (RFC 6724, section 5).")
 #define DESCRIBE_DOC_NM_SETTING_IP6_CONFIG_DAD_TIMEOUT N_("Timeout in milliseconds used to check for the presence of duplicate IP addresses on the network.  If an address conflict is detected, the activation will fail.  A zero value means that no duplicate address detection is performed, -1 means the default value (either configuration ipvx.dad-timeout override or zero).  A value greater than zero is a timeout in milliseconds. The property is currently implemented only for IPv4.")
 #define DESCRIBE_DOC_NM_SETTING_IP6_CONFIG_DHCP_DUID N_("A string containing the DHCPv6 Unique Identifier (DUID) used by the dhcp client to identify itself to DHCPv6 servers (RFC 3315). The DUID is carried in the Client Identifier option. If the property is a hex string ('aa:bb:cc') it is interpreted as a binary DUID and filled as an opaque value in the Client Identifier option. The special value \"lease\" will retrieve the DUID previously used from the lease file belonging to the connection. If no DUID is found and \"dhclient\" is the configured dhcp client, the DUID is searched in the system-wide dhclient lease file. If still no DUID is found, or another dhcp client is used, a global and permanent DUID-UUID (RFC 6355) will be generated based on the machine-id. The special values \"llt\" and \"ll\" will generate a DUID of type LLT or LL (see RFC 3315) based on the current MAC address of the device. In order to try providing a stable DUID-LLT, the time field will contain a constant timestamp that is used globally (for all profiles) and persisted to disk. The special values \"stable-llt\", \"stable-ll\" and \"stable-uuid\" will generate a DUID of the corresponding type, derived from the connection's stable-id and a per-host unique key. You may want to include the \"${DEVICE}\" or \"${MAC}\" specifier in the stable-id, in case this profile gets activated on multiple devices. So, the link-layer address of \"stable-ll\" and \"stable-llt\" will be a generated address derived from the stable id. The DUID-LLT time value in the \"stable-llt\" option will be picked among a static timespan of three years (the upper bound of the interval is the same constant timestamp used in \"llt\"). When the property is unset, the global value provided for \"ipv6.dhcp-duid\" is used. If no global value is provided, the default \"lease\" value is assumed.")
 #define DESCRIBE_DOC_NM_SETTING_IP6_CONFIG_DHCP_HOSTNAME N_("If the \"dhcp-send-hostname\" property is TRUE, then the specified name will be sent to the DHCP server when acquiring a lease. This property and \"dhcp-fqdn\" are mutually exclusive and cannot be set at the same time.")
diff --git a/src/nmcli/generate-docs-nm-settings-nmcli.xml.in b/src/nmcli/generate-docs-nm-settings-nmcli.xml.in
index 525b36b634cb..f826e6b99442 100644
--- a/src/nmcli/generate-docs-nm-settings-nmcli.xml.in
+++ b/src/nmcli/generate-docs-nm-settings-nmcli.xml.in
@@ -712,7 +712,7 @@
                   description="DNS servers priority. The relative priority for DNS servers specified by this setting.  A lower numerical value is better (higher priority). Negative values have the special effect of excluding other configurations with a greater numerical priority value; so in presence of at least one negative priority, only DNS servers from connections with the lowest priority value will be used. To avoid all DNS leaks, set the priority of the profile that should be used to the most negative value of all active connections profiles. Zero selects a globally configured default value. If the latter is missing or zero too, it defaults to 50 for VPNs (including WireGuard) and 100 for other connections. Note that the priority is to order DNS settings for multiple active connections.  It does not disambiguate multiple DNS servers within the same connection profile. When multiple devices have configurations with the same priority, VPNs will be considered first, then devices with the best (lowest metric) default route and then all other devices. When using dns=default, servers with higher priority will be on top of resolv.conf. To prioritize a given server over another one within the same connection, just specify them in the desired order. Note that commonly the resolver tries name servers in /etc/resolv.conf in the order listed, proceeding with the next server in the list on failure. See for example the &quot;rotate&quot; option of the dns-options setting. If there are any negative DNS priorities, then only name servers from the devices with that lowest priority will be considered. When using a DNS resolver that supports Conditional Forwarding or Split DNS (with dns=dnsmasq or dns=systemd-resolved settings), each connection is used to query domains in its search list. The search domains determine which name servers to ask, and the DNS priority is used to prioritize name servers based on the domain.  Queries for domains not present in any search list are routed through connections having the &apos;~.&apos; special wildcard domain, which is added automatically to connections with the default route (or can be added manually).  When multiple connections specify the same domain, the one with the best priority (lowest numerical value) wins.  If a sub domain is configured on another interface it will be accepted regardless the priority, unless parent domain on the other interface has a negative priority, which causes the sub domain to be shadowed. With Split DNS one can avoid undesired DNS leaks by properly configuring DNS priorities and the search domains, so that only name servers of the desired interface are configured." />
         <property name="addresses"
                   alias="ip6"
-                  description="A list of IPv6 addresses and their prefix length. Multiple addresses can be separated by comma. For example &quot;2001:db8:85a3::8a2e:370:7334/64, 2001:db8:85a3::5/64&quot;. The addresses are listed in decreasing priority, meaning the first address will be the primary address. This can make a difference with IPv6 source address selection (RFC 6724, section 5)." />
+                  description="A list of IPv6 addresses and their prefix length. Multiple addresses can be separated by comma. For example &quot;2001:db8:85a3::8a2e:370:7334/64, 2001:db8:85a3::5/64&quot;. The addresses are listed in increasing priority, meaning the last address will be the primary address. This can make a difference with IPv6 source address selection (RFC 6724, section 5)." />
         <property name="gateway"
                   alias="gw6"
                   description="The gateway associated with this configuration. This is only meaningful if &quot;addresses&quot; is also set. The gateway&apos;s main purpose is to control the next hop of the standard default route on the device. Hence, the gateway property conflicts with &quot;never-default&quot; and will be automatically dropped if the IP configuration is set to never-default. As an alternative to set the gateway, configure a static default route with /0 as prefix length." />
-- 
2.35.1

